import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  fruit = '';
  fruits=[];

  onAdd(event) {
    this.fruits.push(this.fruit);
    this.fruit='';
  }
}
